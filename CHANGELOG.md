# [3.3.0](https://gitlab.com/dreamer-labs/piperci/python-piperci/compare/v3.2.1...v3.3.0) (2019-11-26)


### Features

* add get_health ([592df2a](https://gitlab.com/dreamer-labs/piperci/python-piperci/commit/592df2a))

## [3.2.1](https://gitlab.com/dreamer-labs/piperci/python-piperci/compare/v3.2.0...v3.2.1) (2019-11-22)


### Bug Fixes

* Removed unused requirements ([6e868a7](https://gitlab.com/dreamer-labs/piperci/python-piperci/commit/6e868a7))

# [3.2.0](https://gitlab.com/dreamer-labs/piperci/python-piperci/compare/v3.1.0...v3.2.0) (2019-11-18)


### Features

* Add get_run and get_thread ([f1bac74](https://gitlab.com/dreamer-labs/piperci/python-piperci/commit/f1bac74))

# [3.1.0](https://gitlab.com/dreamer-labs/piperci/python-piperci/compare/v3.0.1...v3.1.0) (2019-11-14)


### Features

* add get_task function ([a7e105c](https://gitlab.com/dreamer-labs/piperci/python-piperci/commit/a7e105c))

## [3.0.1](https://gitlab.com/dreamer-labs/piperci/python-piperci/compare/v3.0.0...v3.0.1) (2019-11-04)


### Bug Fixes

* Add message back to task.complete ([178367e](https://gitlab.com/dreamer-labs/piperci/python-piperci/commit/178367e))

# [3.0.0](https://gitlab.com/dreamer-labs/piperci/python-piperci/compare/v2.1.0...v3.0.0) (2019-10-29)


### Code Refactoring

* Safer file uploads ([9fe9286](https://gitlab.com/dreamer-labs/piperci/python-piperci/commit/9fe9286))


### BREAKING CHANGES

* upload functions require is_path=True for pathlike
uploads

# [2.1.0](https://gitlab.com/dreamer-labs/piperci/python-piperci/compare/v2.0.0...v2.1.0) (2019-10-25)


### Features

* allow user to get_source ([5378577](https://gitlab.com/dreamer-labs/piperci/python-piperci/commit/5378577))

# [2.0.0](https://gitlab.com/dreamer-labs/piperci/python-piperci/compare/v1.1.0...v2.0.0) (2019-10-25)


### Features

* support for gman artifacts and return_code for events ([6d17c58](https://gitlab.com/dreamer-labs/piperci/python-piperci/commit/6d17c58))


### BREAKING CHANGES

* gman.client methods only allow keyword parameters

# [1.1.0](https://gitlab.com/dreamer-labs/piperci/python-piperci/compare/v1.0.0...v1.1.0) (2019-10-21)


### Features

* Allow named file uploads ([913fce8](https://gitlab.com/dreamer-labs/piperci/python-piperci/commit/913fce8))

# 1.0.0 (2019-10-17)


### Features

* Add dynamic upload methods to ThisTask ([aea424b](https://gitlab.com/dreamer-labs/piperci/python-piperci/commit/aea424b))


### BREAKING CHANGES

* Removed "logfile" method and replaced with "log"

Adds a method to dynamically bind methods to ThisTask to be used
to upload output from a FaaS to MinIO. The methods are bound based
on a list defined in artman_client. This allows us to dynamically
define methods based on an external list, adding and removing new
methods based on the defined artifact types.

To add a new artifact type that ThisTask would understand, you
would just add to the list defined in piperci.artman.artman_client
in the artifact_types function. This would be used when a ThisTask
object is initialized to create additional methods for wrapping
upload.
