class PiperError(Exception):
    pass


class PiperDelegateError(PiperError):
    pass


class PiperActiveError(PiperError):
    pass


class PiperLoggingError(PiperError):
    pass
