import io
import json
import logging
import os
import tarfile
import tempfile
import zipfile

import requests

from pythonjsonlogger import jsonlogger

from piperci.task.exceptions import (
    PiperActiveError,
    PiperDelegateError,
    PiperLoggingError,
    PiperError,
)
from piperci.artman import artman_client, exceptions as artman_exceptions
from piperci.gman import client as gman_client
from piperci.sri import generate_sri
from piperci.storeman.client import storage_client
from piperci.storeman.exceptions import StoremanError


class ThisTask:
    def __init__(
        self,
        gman_url,
        *,
        storage,
        project,
        thread_id=None,
        parent_id=None,
        run_id,
        status,
        stage,
        caller,
        log_file="piperci_task.log",
    ):
        """
        Representation of a task object from GMan.
        :param gman_url: URL for GMan
        :param storage: Dict containing storage parameters
        :param project: Project name
        :param thread_id: Thread to associate this task with
        :param parent_id: Parent task ID to associate this task with
        :param run_id: Run to associate this task with
        :param status: Status of the task. Must be "started" or "received"
        :param stage: The name of the stage that this task is associated with.
        :param caller: The name of the program that created this task.
        """
        self.gman_url = gman_url
        self.stage = stage
        self.project = project
        self.thread_id = thread_id
        self.parent_id = parent_id
        if run_id:
            self.run_id = str(run_id)
        else:
            raise PiperActiveError("run_id must not be None")
        self.status = status
        self.caller = caller
        self.log_file = log_file
        self.logger = self._init_logger(log_file=self.log_file)
        self.storage_client = self._init_storage_client(**storage)
        self.task = self._start(
            project=project,
            thread_id=thread_id,
            parent_id=parent_id,
            run_id=run_id,
            status=status,
            caller=caller,
        )
        self.task_id = self.task["task"]["task_id"]
        self._set_artifact_types()
        self._unique_file = 0

    def _set_artifact_types(self):
        """
        Dynamically binds methods to ThisTask based on the allowed
        types of artifacts from artman_client.
        Example: If the allowed artifact_types are ['stdout', 'stderr']
        then we will create and bind a `stdout` and `stderr` method.
        :return: None
        """
        artifact_types = artman_client.artifact_types()
        for artifact_type in artifact_types:

            def func(self, content, filetype=artifact_type, name=None, is_path=False):
                try:
                    return self._upload(content=content,
                                        filetype=filetype,
                                        upload_as=name,
                                        is_path=is_path)
                except Exception:
                    self.info("failed to upload {artifact_type}")
                    raise PiperLoggingError("failed to upload {artifact_type}")

            setattr(self, artifact_type, func.__get__(self, self.__class__))

    def _init_logger(self, log_level="info", log_file="piperci_task.log"):
        """
        Initializes the Python logger
        :param log_level: Sets the log level for all handlers
        :return: Reference to the logger
        """
        logger = logging.getLogger(__name__)
        logger.setLevel(log_level.upper())

        stream = logging.StreamHandler()
        stream.setLevel(log_level.upper())
        formatter = jsonlogger.JsonFormatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s", timestamp=True
        )
        stream.setFormatter(formatter)

        file = logging.FileHandler(log_file)
        file.setLevel(log_level.upper())
        formatter = jsonlogger.JsonFormatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s", timestamp=True
        )
        file.setFormatter(formatter)

        logger.addHandler(stream)
        logger.addHandler(file)

        return logger

    def _init_storage_client(self, storage_type, **kwargs):
        """
        Initializes the storage client.
        :param storage_type: The type of storage that will be used
        :param kwargs: Required KWargs depend on the storage type.
          For Minio:
          hostname: The hostname of the storage service.
          access_key: The access key for the storage service.
          secret_key: The secret key for the storage service
        :return: piperci.storeman.client.BaseStorageClient
        """

        minio_client = storage_client(
            storage_type=storage_type,
            hostname=kwargs.get("hostname"),
            access_key=kwargs.get("access_key"),
            secret_key=kwargs.get("secret_key"),
            secure=False,
        )
        return minio_client

    def _start(self, *, project, thread_id, parent_id, run_id, status, caller):
        """
        Requests a task from GMan
        :param project: The project name that the task is associated with.
        :param thread_id: The thread that the task is associated with. Can be none.
        :param parent_id: The parent task ID. Can be none.
        :param status: The status to initialize the task with.
        Must be "received" or "started"
        :param caller: The caller of the task.
        :return: Task object from GMan
        """
        self.logger.info(f"Requesting task from GMan")

        data = {
            "gman_url": self.gman_url,
            "run_id": run_id,
            "project": project,
            "thread_id": thread_id,
            "status": status,
            "caller": caller,
        }
        if parent_id:
            data.update({"parent_id": parent_id})

        self.logger.debug(f"Params {data}")
        try:
            task = gman_client.request_new_task_id(**data)
            self.logger.info(f"Received new task {task}")
            return task
        except requests.RequestException as e:
            self.logger.error(
                f"Encountered unknown requests exception while trying to "
                f"request new task. {e}"
            )
            raise PiperActiveError(e)
        except ValueError as e:
            self.logger.error(
                f"ValueError received while trying to request a new task. {e}"
            )
            raise PiperActiveError(e)

    def _upload(self, *, content, filetype="log",
                upload_as=None, is_path=False):
        """
        Uploads a Filelike/string/file_path/folder to Storage a storage backend.
        :param content: File like, string, or path to be uploaded
        :param filetype: The type of the file. "log" or "artifact"
        :param upload_as: The destination name of the file,
                          if not specified one will be generated.
        :param is_path: bool the content is a valid path not open file or string
        :return:
        """
        if is_path:
            upload_as = self.gen_file_name(basename=os.path.basename(content))
            if os.path.isdir(content):
                artifacts = []
                for root, dir, files in os.walk(content):
                    for file in files:
                        artifacts.append(self._upload_file(file,
                                                           filetype=filetype,
                                                           name=upload_as))
                return artifacts
            elif os.path.isfile(content):
                return self._upload_file(content, filetype=filetype, name=upload_as)
            else:
                raise ValueError(f"Invalid path: {content}")
        else:
            upload_as = self.gen_file_name(basename=upload_as)
            with tempfile.NamedTemporaryFile() as temp:
                if isinstance(content, str):
                    content = io.BytesIO(content.encode())

                try:
                    temp.write(content.read())
                except IOError as e:
                    raise PiperLoggingError(f"Error writing raw file to {temp.name}:"
                                            f"{str(e)}")

                return self._upload_file(temp.name, filetype=filetype, name=upload_as)

    def _upload_file(self, path, *, filetype, name=None):
        """
        Uploads a file to the storage backend and tells ArtMan that a file
        has been uploaded with a given URI
        :param file: The path to the file that is being uploaded
        :param filetype: The type of the file. "log" or "artifact"
        :return: None
        """
        if not name:
            name = path

        try:
            self.logger.info(
                f"Uploading file {path} as {name} to"
                f"{self.get_artifact_uri(name, filetype)}"
            )
            bucket, object_name = self.get_artifact_uri(name, filetype, False)
            self.storage_client.upload_file(bucket, object_name, path)

            self.info(f"Uploaded file {path} as {name}")
            project_artifact_hash = generate_sri(path)
            artifact_uri = self.get_artifact_uri(name, filetype)
            artifact = artman_client.post_artifact(
                task_id=self.task_id,
                artman_url=self.gman_url,
                uri=artifact_uri,
                caller=self.caller,
                type=filetype,
                sri=str(project_artifact_hash),
            )
            self.logger.info(f"Artifact uploaded at {artifact_uri}")
            return artifact
        except StoremanError as e:
            self.logger.error(
                f"Encountered unknown exception while trying to upload files to storage."
                f"{e}"
            )
            raise PiperLoggingError(e)
        except artman_exceptions.ArtmanRequestException as e:
            message = (
                f"Encountered unknown requests exception while trying to"
                f"post artifact to ArtMan. {e}"
            )
            self.logger.error(message)
            raise PiperLoggingError(e)

    @property
    def short_id(self):
        return self.task_id[:8]

    def complete(self, message, return_code=None):
        """
        Completes the task with a given message
        :param message: Message to send to GMan on task completion.
        :return: A reference to the task and 200
        """
        self.logger.info(message)
        try:
            task_id = self.task_id
            self._upload_file(self.log_file, filetype="log")
            gman_update_args = {
                "task_id": task_id,
                "gman_url": self.gman_url,
                "status": "completed",
                "message": message
            }
            if return_code is not None:
                gman_update_args.update({"return_code": return_code})
            gman_client.update_task_id(**gman_update_args)
            return json.dumps(self.task), 200
        except requests.exceptions.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception when trying to complete task. {e}"
            )
            raise PiperLoggingError(e)

    def delegate(self, url, data):
        """
        Attempts to delegate a task to another PiperCI Task, usually an executor
        Raises PiperDelegateError if this fails
        Marks a task as delegated
        :return: None
        """
        self.info(f"Attempting to delegate execution to {url}")
        headers = {"Content-Type": "application/json"}
        data["stage"] = self.stage
        data.update(self.task["task"])
        data["parent_id"] = self.task_id
        data["thread_id"] = self.task["task"]["thread_id"]
        try:
            r = requests.post(url, data=json.dumps(data), headers=headers)
            r.raise_for_status()
        except requests.exceptions.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception when trying to delegate task. {e}"
            )
            raise PiperDelegateError(e)

        try:
            gman_client.update_task_id(
                task_id=self.task_id,
                gman_url=self.gman_url,
                status="delegated",
                message=f"delegated execution to {url}",
            )
        except requests.exceptions.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception when updating task"
            )
            raise PiperDelegateError(e)

        self.info(f"Successfully delegated task {self.task} to {url}.")

    def fail(self, message, return_code=None):
        """
        Marks a task as failed in GMan and sends 422 to client
        :param message: Message to pass to the logger and GMan
        :return:
        """
        self.logger.error(message)
        try:
            self._upload_file(self.log_file, filetype="log")
            gman_update_args = {
                "gman_url": self.gman_url,
                "status": "failed",
                "task_id": self.task_id,
                "message": message,
            }
            if return_code:
                gman_update_args.update({"return_code": return_code})
            gman_client.update_task_id(**gman_update_args)
        except requests.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception when trying to mark task failed."
                f"{e}"
            )
            raise PiperLoggingError(e)
        return message, 422

    def get_source(self, *, sri, extract_to="source"):
        """
        Queries ArtMan and downloads a source artifact from the storage backend.
        :return:
        """
        self.logger.info("Querying ArtMan for source artifact")

        source_artifact = next(
            iter(artman_client.get_artifact(artman_url=self.gman_url, sri_urlsafe=sri)),
            None,
        )

        if not source_artifact:
            raise PiperError(f"No source artifact found with sri {sri}")

        self.info(
            f"Downloading source artifact {source_artifact.get('uri')} from storage",
            artifact_id=source_artifact.get("artifact_id"),
        )
        with tempfile.TemporaryDirectory() as tmpdir:
            tmp_download_file = f"{tmpdir}/{extract_to}"
            self.storage_client.download_file(
                source_artifact.get("uri"), tmp_download_file
            )

            if zipfile.is_zipfile(tmp_download_file):
                self.logger.info(
                    f"Detected source artifact was a zipfile. Unzipping to {extract_to}"
                )
                with zipfile.ZipFile(tmp_download_file, "r") as zip_ref:
                    zip_ref.extractall(extract_to)
            elif tarfile.is_tarfile(tmp_download_file):
                self.logger.info(
                    f"Detected source artifact was a tarfile. Extract to {extract_to}"
                )
                with tarfile.open(tmp_download_file) as tar_ref:
                    tar_ref.extractall(extract_to)

    def info(self, message, artifact_id=None, return_code=None):
        """
        Logs an info message to GMan
        :param message: Message to log
        :param artifact_id: Artifact to associate
        :return:
        """
        self.logger.info(message)
        gman_update_args = {
            "gman_url": self.gman_url,
            "status": "info",
            "task_id": self.task_id,
            "message": message
        }
        if artifact_id:
            gman_update_args.update({"artifact_id": artifact_id})
        if return_code:
            gman_update_args.update({"return_code": return_code})
        try:
            gman_client.update_task_id(**gman_update_args)
        except requests.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception while trying to log to GMan. {e}"
            )
            raise PiperLoggingError(e)

    def get_artifact_uri(self, file, filetype, fqdn=True):
        hyphenate_bad_chars = str.maketrans("_ ", "--")
        container = f"run-{self.run_id.translate(hyphenate_bad_chars)}"
        object_name = f"{filetype}/{self.stage}/{file}"
        if fqdn:
            return self.storage_client.gen_file_uri(container, object_name)
        else:
            return (container, object_name)

    def gen_file_name(self, basename=''):
        if not basename:
            self._unique_file += 1
            basename = f"{self._unique_file}-out"
        return f"{self.short_id}-{basename}"
