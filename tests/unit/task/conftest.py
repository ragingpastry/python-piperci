import pytest


@pytest.fixture(scope="session")
def log_directory_fixture(tmpdir_factory):
    logdir = tmpdir_factory.mktemp("logs")
    for i in range(0, 5):
        log_file = logdir.join(f"log_file_{i}.txt")
        log_file.write("logs")
    return str(logdir)


@pytest.fixture(scope="session")
def log_file_fixture(tmpdir_factory):
    log_file = tmpdir_factory.mktemp("logs").join("log_file.txt")
    log_file.write("logs")
    return str(log_file)


@pytest.fixture
def request_new_task_patch(mocker):
    mocker.patch("piperci.gman.client.request_new_task_id")


@pytest.fixture
def requests_post_patch(mocker):
    mocker.patch("piperci.task.this_task.requests.post")


@pytest.fixture
def upload_file_patch_to_noop(mocker):
    mocker.patch("piperci.storeman.minio_client.MinioClient.upload_file")


@pytest.fixture
def download_file_patch(mocker):
    mocker.patch("piperci.storeman.minio_client.MinioClient.download_file")


@pytest.fixture
def artman_patch(mocker):
    mocker.patch("piperci.artman.artman_client.post_artifact")


@pytest.fixture
def update_task_id_patch(mocker):
    mocker.patch("piperci.gman.client.update_task_id")


@pytest.fixture
def storage_fixture():
    storage = {
        "storage_type": "minio",
        "hostname": "localhost",
        "access_key": "1234",
        "secret_key": "1234",
    }
    return storage


@pytest.fixture
def artifact():
    data = {
        "status": "unknown",
        "uri": "minio://someminio.example.com/art1",
        "artifact_id": "884053a3-277b-45e4-9813-fc61c07a2cd6",
        "type": "artifact",
        "sri": "sha256-sCDaaxdshXhK4sA/v4dMHiMWhtGyQwA1fP8PgrN0O5g=",
        "task": {
            "task_id": "a9a1ca15-747d-43f9-8f04-1a66de8fef33",
            "caller": "test_case_create_1",
            "project": "gman_test_data",
            "thread_id": "a9a1ca15-747d-43f9-8f04-1a66de8fef33",
            "run_id": "create_1",
        },
        "event_id": "a48efe28-db9e-4330-93c4-5f480b2bef71",
    }
    return [data]


@pytest.fixture
def task_response():
    return {
        "task": {
            "project": "test",
            "task_id": "1234",
            "caller": "python",
            "thread_id": "1234",
            "run_id": "1234",
        },
        "timestamp": "2019-06-07T16:58:20.513731+00:00",
        "message": "Requesting new taskID",
        "status": "completed",
    }
