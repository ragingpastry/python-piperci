import logging
import pathlib
import os.path

from functools import partial

import mock
import pytest
import requests
import responses

from mock import ANY

from piperci.task.exceptions import (
    PiperActiveError,
    PiperDelegateError,
    PiperLoggingError,
    PiperError,
)
from piperci.artman.artman_client import artifact_types
from piperci.artman.exceptions import ArtmanRequestException
from piperci.task.this_task import ThisTask
from piperci.storeman.exceptions import StoremanError
from piperci.storeman.minio_client import MinioClient


cached_task = None


def thistask(new=False):
    global cached_task

    if cached_task:
        return cached_task
    else:
        with mock.patch(
            "piperci.gman.client.request_new_task_id",
            return_value={"task": {"task_id": "12abc", "thread_id": "12abc"}},
        ):
            storage = {
                "storage_type": "minio",
                "hostname": "localhost",
                "access_key": "1234",
                "secret_key": "1234",
            }
            data = {
                "project": "test",
                "run_id": "12abc",
                "status": "started",
                "caller": "pytest",
                "stage": "test",
            }
            cached_task = ThisTask("http://localhost:8089", storage=storage, **data)
            return cached_task


@pytest.fixture
def this_task():
    global cached_task
    cached_task = None
    return thistask


def test_run_id_none():

    with mock.patch(
        "piperci.gman.client.request_new_task_id",
        return_value={"task": {"task_id": "12abc"}},
    ):
        storage = {
            "storage_type": "minio",
            "hostname": "localhost",
            "access_key": "12abc",
            "secret_key": "12abc",
        }
        data = {
            "project": "test",
            "run_id": None,
            "status": "started",
            "caller": "pytest",
            "stage": "test",
        }
    with pytest.raises(PiperActiveError):
        ThisTask("http://localhost:8089", storage=storage, **data)


def test_logger_is_a_logger(this_task):
    assert isinstance(this_task().logger, logging.Logger)


@pytest.mark.usefixtures(
    "request_new_task_patch",
    "upload_file_patch_to_noop",
    "update_task_id_patch",
    "requests_post_patch",
)
@pytest.mark.parametrize(
    "options",
    [
        (partial(thistask().artifact, "path"), "info"),
        (partial(thistask().log, "path"), "info"),
        (partial(thistask().complete, "message"), "info"),
        (partial(thistask().delegate, "url", {"data": None}), "info"),
        (partial(thistask().fail, "message"), "error"),
        (partial(thistask().info, "message"), "info"),
        (
            partial(
                thistask()._start,
                **{
                    "run_id": None,
                    "project": "test",
                    "parent_id": None,
                    "thread_id": None,
                    "status": "started",
                    "caller": "pytest",
                },
            ),
            "info",
        ),
    ],
)
def test_calls_logger(options, mocker):
    logger = mocker.patch(f"logging.Logger.{options[1]}")
    options[0]()
    logger.assert_called()


def test_storage_client(this_task):
    assert isinstance(this_task().storage_client, MinioClient)


def test_start_passes_parent_id(mocker, storage_fixture):
    mock_request = mocker.patch("piperci.gman.client.request_new_task_id")
    data = {
        "project": "test",
        "run_id": "12abc",
        "status": "started",
        "caller": "pytest",
        "parent_id": "12abc",
        "stage": "test",
    }
    ThisTask("http://localhost:8089", storage=storage_fixture, **data)
    data.update({"gman_url": "http://localhost:8089"})
    assert mock_request.called_once_with(**data)


@pytest.mark.parametrize("exception", [requests.RequestException, ValueError])
def test_start_raises_piper_active_error(exception, mocker, storage_fixture):
    mocker.patch("piperci.gman.client.request_new_task_id", side_effect=exception)
    data = {
        "project": "test",
        "run_id": "12abc",
        "status": "started",
        "caller": "pytest",
        "parent_id": "12abc",
        "stage": "test",
    }
    with pytest.raises(PiperActiveError):
        ThisTask("http://localhost:8089", storage=storage_fixture, **data)


def test_delegate_task(this_task, mocker):
    mock_request = mocker.patch("piperci.task.this_task.requests.post")
    mocker.patch("piperci.task.this_task.ThisTask.info")
    mock_gman = mocker.patch("piperci.gman.client.update_task_id")
    data = {
        "run_id": "12abc",
        "thread_id": "12abc",
        "project": "12abc",
        "configs": "12abc",
        "stage": "12abc",
        "artifacts": "12abc",
    }
    this_task().delegate("http://my-async-func", data)
    mock_request.assert_called_once()
    mock_gman.assert_called_once()


def test_delegate_task_raises_delegate_error_on_exception(this_task, mocker):
    mocker.patch(
        "piperci.task.this_task.requests.post",
        side_effect=requests.exceptions.HTTPError,
    )
    mocker.patch("piperci.task.this_task.ThisTask.info")

    data = {
        "run_id": "12abc",
        "thread_id": "12abc",
        "project": "12abc",
        "configs": "12abc",
        "stage": "12abc",
        "artifacts": "12abc",
    }
    with pytest.raises(PiperDelegateError):
        this_task().delegate("http://my-async-func", data)


def test_delegate_task_raises_delegate_error_on_gman_exception(this_task, mocker):
    mocker.patch(
        "piperci.gman.client.update_task_id",
        side_effect=requests.exceptions.RequestException,
    )
    mocker.patch("piperci.task.this_task.requests.post")
    mocker.patch("piperci.task.this_task.ThisTask.info")
    data = {
        "run_id": "12abc",
        "thread_id": "12abc",
        "project": "12abc",
        "configs": "12abc",
        "stage": "12abc",
        "artifacts": "12abc",
    }
    with pytest.raises(PiperDelegateError):
        this_task().delegate("http://my-async-func", data)


@pytest.mark.usefixtures("upload_file_patch_to_noop", "update_task_id_patch")
@pytest.mark.parametrize("filetype", ["log", "artifact"])
def test_upload_file_calls_post_artifact_with_correct_type(this_task, filetype, mocker):
    mocker.patch("piperci.task.this_task.generate_sri", return_value="1234")
    mock_post_artifact = mocker.patch(
        "piperci.task.this_task.artman_client.post_artifact"
    )
    this_task()._upload_file("mytestfile", filetype=filetype)
    expected_args = {
        "task_id": this_task().task["task"]["task_id"],
        "artman_url": this_task().gman_url,
        "uri": f"minio://{this_task().storage_client.hostname}"
        f"/run-{this_task().run_id}/{filetype}/{this_task().stage}"
        f"/mytestfile",
        "caller": this_task().caller,
        "type": filetype,
        "sri": "1234",
    }

    mock_post_artifact.assert_called_once_with(**expected_args)


@pytest.mark.parametrize("filetype", ["artifact", "log"])
@pytest.mark.parametrize(
    "run_id,expected",
    [("_1", "run--1"), (" 23", "run--23"), ("1234", "run-1234"), ("_ ", "run---")],
)
def test_get_artifact_uri_fqdn_bad_buckets(this_task, filetype, run_id, expected):
    task = this_task()
    task.run_id = run_id
    uri = task.get_artifact_uri("somefilename", filetype)
    assert expected in uri


@pytest.mark.parametrize("filetype", ["artifact", "log"])
@pytest.mark.parametrize(
    "run_id,expected",
    [("_1", "run--1"), (" 23", "run--23"), ("1234", "run-1234"), ("_ ", "run---")],
)
def test_get_artifact_uri_bad_buckets(this_task, filetype, run_id, expected):
    task = this_task()
    task.run_id = run_id
    container, obj_name = task.get_artifact_uri("somefilename", filetype, fqdn=False)
    assert expected == container


@pytest.mark.usefixtures("upload_file_patch_to_noop", "update_task_id_patch")
def test_upload_file_raises_piperloggingerror(this_task, mocker):
    mocker.patch("piperci.task.this_task.generate_sri", return_value="12abc")
    mocker.patch(
        "piperci.task.this_task.artman_client.post_artifact",
        side_effect=ArtmanRequestException,
    )
    with pytest.raises(PiperLoggingError):
        this_task()._upload_file("mytestfile", filetype="log")


@pytest.mark.usefixtures("upload_file_patch_to_noop", "update_task_id_patch")
def test_upload_file_storeman_error_raises_piperloggingerror(this_task, mocker):
    mocker.patch("piperci.task.this_task.generate_sri", return_value="12abc")
    mocker.patch(
        "piperci.storeman.minio_client.MinioClient.upload_file",
        side_effect=StoremanError,
    )
    with pytest.raises(PiperLoggingError):
        this_task()._upload_file("mytestfile", filetype="log")


@pytest.mark.usefixtures("update_task_id_patch")
def test_artifact_descends_into_directory(this_task, mocker, log_directory_fixture):
    mock_upload = mocker.patch.object(ThisTask, "_upload_file")
    this_task().artifact(log_directory_fixture, is_path=True)

    assert mock_upload.call_count == 5
    assert_name = f"{this_task().short_id}-{os.path.basename(log_directory_fixture)}"
    mock_upload.assert_called_with(ANY, filetype="artifact", name=assert_name)


@pytest.mark.usefixtures("update_task_id_patch")
def test_artifact_uploads_file(this_task, mocker, log_file_fixture):
    mock_upload = mocker.patch.object(ThisTask, "_upload_file")
    this_task().artifact(log_file_fixture, is_path=True)

    assert mock_upload.call_count == 1
    assert_name = f"{this_task().short_id}-{os.path.basename(log_file_fixture)}"
    mock_upload.assert_called_with(
        log_file_fixture, filetype="artifact", name=assert_name
    )


@pytest.mark.usefixtures("update_task_id_patch")
def test_logfile_descends_into_directory(this_task, mocker, log_directory_fixture):
    mock_upload = mocker.patch.object(ThisTask, "_upload_file")
    this_task().log(log_directory_fixture, is_path=True)

    assert mock_upload.call_count == 5
    assert_name = f"{this_task().short_id}-{os.path.basename(log_directory_fixture)}"
    mock_upload.assert_called_with(ANY, filetype="log", name=assert_name)


@pytest.mark.usefixtures("update_task_id_patch")
def test_logfile_uploads_file(this_task, mocker, log_file_fixture):
    this_task = this_task()
    mock_upload = mocker.patch.object(ThisTask, "_upload_file")
    this_task.log(log_file_fixture, is_path=True)

    assert mock_upload.call_count == 1
    mock_upload.assert_called_with(
        log_file_fixture,
        filetype="log",
        name=f"{this_task.short_id}-{os.path.basename(log_file_fixture)}",
    )


@pytest.mark.usefixtures("update_task_id_patch")
@pytest.mark.parametrize("log_type", ["stdout", "stderr"])
def test_log_output_descends_into_directory(
    log_type, this_task, mocker, log_directory_fixture
):
    mock_upload = mocker.patch.object(ThisTask, "_upload_file")
    func = getattr(this_task(), log_type)
    func(log_directory_fixture, is_path=True)
    assert mock_upload.call_count == 5
    assert_name = f"{this_task().short_id}-{os.path.basename(log_directory_fixture)}"
    mock_upload.assert_called_with(ANY, filetype=log_type, name=assert_name)


@pytest.mark.usefixtures("update_task_id_patch")
@pytest.mark.parametrize("log_type", ["stdout", "stderr"])
def test_log_output_uploads_file(log_type, this_task, mocker, log_file_fixture):
    this_task = this_task()
    mock_upload = mocker.patch.object(ThisTask, "_upload_file")
    func = getattr(this_task, log_type)
    func(log_file_fixture, is_path=True)
    assert mock_upload.call_count == 1
    assert_name = f"{this_task.short_id}-{os.path.basename(log_file_fixture)}"
    mock_upload.assert_called_with(
        log_file_fixture, filetype=log_type, name=assert_name
    )


@pytest.mark.usefixtures("update_task_id_patch")
@pytest.mark.parametrize("log_type", ["stdout", "stderr"])
def test_log_output_no_file_or_directory_calls_info(log_type, this_task, mocker):
    mock_upload = mocker.patch.object(ThisTask, "_upload_file")
    func = getattr(this_task(), log_type)
    with pytest.raises(PiperLoggingError):
        func(pathlib.Path("does_not_exist"), is_path=True)
    assert mock_upload.call_count == 0


@pytest.mark.usefixtures(
    "request_new_task_patch",
    "upload_file_patch_to_noop",
    "update_task_id_patch",
    "requests_post_patch",
    "artman_patch",
)
@pytest.mark.parametrize(
    "func",
    [
        (partial(thistask()._upload_file, "path", filetype="log"), "info"),
        (partial(thistask().complete, "message"), "info"),
        (partial(thistask().delegate, "url", "data"), "info"),
        (partial(thistask().fail, "message"), "error"),
        (partial(thistask().info, "message"), "info"),
    ],
)
def test_exception_logs_error(func, mocker):
    mocker.patch(
        "piperci.gman.client.update_task_id", side_effect=requests.RequestException
    )
    logger = mocker.patch(f"logging.Logger.error")

    with pytest.raises(PiperLoggingError):
        func[0]()
        logger.assert_called_once()


def test_io_exception_tempfile(this_task, mocker):

    thistask = this_task()

    def my_func(*args, **kwargs):
        class Thing(object):
            name = "mocked tempfile"

            def write(self, *args, **kwargs):
                raise IOError("oooh nooo")

            def __enter__(self):
                return self

            def __exit__(self, exc_type, exc_val, exc_tb):
                pass

        return Thing()

    mocker.patch("tempfile.NamedTemporaryFile", my_func)

    with pytest.raises(PiperLoggingError):
        thistask._upload(content="stuff in string")


@pytest.mark.usefixtures("upload_file_patch_to_noop", "artman_patch")
@responses.activate
def test_info(this_task, task_response):
    responses.add(
        responses.PUT,
        "http://localhost:8089/task/12abc",
        status=200,
        json=task_response,
    )
    this_task().info("I failed!")


@pytest.mark.usefixtures(
    "update_task_id_patch", "upload_file_patch_to_noop", "artman_patch"
)
def test_info_passes_return_code(this_task, mocker):
    mock_info = mocker.patch("piperci.gman.client.update_task_id")
    this_task().info("I failed!", return_code=2)

    assert "return_code" in [
        elem for sublist in mock_info.mock_calls for elem, _ in sublist[2].items()
    ]


def test_info_failure_raises_piperloggingerror(this_task, mocker):
    mocker.patch(
        "piperci.gman.client.update_task_id", side_effect=requests.RequestException
    )

    with pytest.raises(PiperLoggingError):
        this_task().info("This is info!")


@pytest.mark.usefixtures("upload_file_patch_to_noop", "artman_patch")
@responses.activate
def test_fail_sends_422_to_client(this_task, task_response):
    responses.add(
        responses.PUT,
        "http://localhost:8089/task/12abc",
        status=200,
        json=task_response,
    )
    failure = this_task().fail("I failed!")

    assert 422 in failure


@pytest.mark.usefixtures("upload_file_patch_to_noop", "artman_patch")
def test_fail_logs_to_gman(this_task, mocker):
    this_task = this_task()
    mock_update = mocker.patch("piperci.gman.client.update_task_id")
    mocker.patch.object(this_task, "_upload_file")

    this_task.fail("I failed!")

    mock_update.assert_called_once()


@pytest.mark.usefixtures(
    "update_task_id_patch", "upload_file_patch_to_noop", "artman_patch"
)
def test_fail_passes_return_code(this_task, mocker):
    mock_update = mocker.patch("piperci.gman.client.update_task_id")
    this_task().fail("I failed!", return_code=2)

    assert "return_code" in [
        elem for sublist in mock_update.mock_calls for elem, _ in sublist[2].items()
    ]


@pytest.mark.usefixtures("upload_file_patch_to_noop")
def test_fail_raises_piperloggingerror(this_task, mocker):
    task = this_task()
    mocker.patch.object(task, "_upload_file")
    mocker.patch(
        "piperci.gman.client.update_task_id", side_effect=requests.RequestException
    )

    with pytest.raises(PiperLoggingError):
        task.fail("I failed!")


@pytest.mark.usefixtures("upload_file_patch_to_noop", "artman_patch")
@responses.activate
def test_complete_sends12abc00_to_client(this_task, task_response):
    responses.add(
        responses.PUT,
        "http://localhost:8089/task/12abc",
        status=200,
        json=task_response,
    )
    completion = this_task().complete("I completed!")

    assert 200 in completion


@pytest.mark.usefixtures(
    "update_task_id_patch", "upload_file_patch_to_noop", "artman_patch"
)
def test_complete_passes_return_code(this_task, mocker):
    mock_update = mocker.patch("piperci.gman.client.update_task_id")
    this_task().complete("I completed!", return_code=2)

    assert "return_code" in [
        elem for sublist in mock_update.mock_calls for elem, _ in sublist[2].items()
    ]


@pytest.mark.usefixtures("upload_file_patch_to_noop", "artman_patch")
def test_complete_logs_to_gman(this_task, mocker):
    task = this_task()
    mocker.patch.object(task, "_upload_file")
    mock_update = mocker.patch("piperci.gman.client.update_task_id")

    task.complete("I completed!")

    mock_update.assert_called_once()


@pytest.mark.usefixtures("upload_file_patch_to_noop")
def test_complete_raises_piperloggingerror(this_task, mocker):
    task = this_task()
    mocker.patch.object(task, "_upload_file")
    mocker.patch(
        "piperci.gman.client.update_task_id", side_effect=requests.RequestException
    )

    with pytest.raises(PiperLoggingError):
        task.complete("I completed!")


def test_this_task_has_bound_methods(this_task):
    task = this_task()
    for art_type in artifact_types():
        assert hasattr(task, art_type)


@pytest.mark.usefixtures("upload_file_patch_to_noop", "update_task_id_patch")
def test_all_uploads_use_name(this_task, mocker):
    mocker.patch("piperci.task.this_task.generate_sri", return_value="1234")
    for art_type in artifact_types():
        mock_post_artifact = mocker.patch(
            "piperci.task.this_task.artman_client.post_artifact"
        )
        func = getattr(this_task(), art_type)
        func("mytestfile", name="test")
        expected_args = {
            "task_id": this_task().task["task"]["task_id"],
            "artman_url": this_task().gman_url,
            "uri": f"minio://{this_task().storage_client.hostname}"
            f"/run-{this_task().run_id}/{art_type}/{this_task().stage}"
            f"/{this_task().task_id}-test",
            "caller": this_task().caller,
            "type": art_type,
            "sri": "1234",
        }
        mock_post_artifact.assert_called_once_with(**expected_args)


@pytest.mark.usefixtures("download_file_patch")
def test_get_source_updates_gman(this_task, mocker, artifact):
    task = this_task()
    mocker.patch(
        "piperci.task.this_task.artman_client._get_artifacts_by_sri",
        return_value=artifact,
    )
    mocker.patch("piperci.task.this_task.tarfile.is_tarfile", return_value=False)
    mock_gman_update = mocker.patch("piperci.gman.client.update_task_id")

    task.get_source(sri="1234")
    mock_gman_update.assert_called_once()


@pytest.mark.usefixtures("update_task_id_patch", "download_file_patch")
def test_get_source_extracts_zipfile(this_task, mocker, artifact):
    task = this_task()
    mocker.patch(
        "piperci.task.this_task.artman_client._get_artifacts_by_sri",
        return_value=artifact,
    )
    mocker.patch("piperci.task.this_task.zipfile.is_zipfile", return_value=True)
    mock_zip = mocker.patch("piperci.task.this_task.zipfile.ZipFile")

    task.get_source(sri="1234")

    mock_zip.assert_called_once()


@pytest.mark.usefixtures("update_task_id_patch", "download_file_patch")
def test_get_source_extracts_tarfile(this_task, mocker, artifact):
    task = this_task()
    mocker.patch(
        "piperci.task.this_task.artman_client._get_artifacts_by_sri",
        return_value=artifact,
    )
    mocker.patch("piperci.task.this_task.tarfile.is_tarfile", return_value=True)
    mock_extract = mocker.patch("piperci.task.this_task.tarfile.open")

    task.get_source(sri="1234")

    mock_extract.assert_called_once()


def test_get_source_raises_pipererror_no_source(this_task, mocker):
    task = this_task()

    mocker.patch(
        "piperci.task.this_task.artman_client._get_artifacts_by_sri", return_value=[]
    )

    with pytest.raises(PiperError):
        task.get_source(sri="1234")
